package wash.org.washapp.maps;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;


public class GPSService  {
    private boolean isGetLocation = false;
    private GPSTracker gps;
    private String TAG = GPSService.class.getSimpleName();
    private Context context;
    private Activity activity;




    public GPSService(Context context, Activity activity) {
        this.context = context;
        this.activity = activity;
    }

    /**
     * Method to display the location on UI
     * */

    public class PushLongLat extends AsyncTask<String,Boolean,String> {

        @Override
        protected void onCancelled(String s) {
            super.onCancelled(s);
        }

        @Override
        protected String doInBackground(String... params) {
            startingGPS();
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
        }
    }

    private void startingGPS(){

        gps= new GPSTracker(context);
        if(isGetLocation()){

            double latitude = getLatitude();
            double longitude = getLongditude();

            Log.d(TAG,"Long : "+longitude+" "+"Lat"+latitude);
            Log.d(TAG,"Starting Gps"+"true");
            // \n is for new line
        }else{
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/network in settings
            Log.d(TAG,"Starting Gps"+"Ask user");
            showSettingAlert();
        }
    }

    private boolean isGetLocation(){

        if(gps.canGetLocation()){
            isGetLocation =  true;

        }
        return isGetLocation;
    }

    private double getLatitude(){
        if(isGetLocation){
            return gps.getLatitude();
        }else{
            return 0;
        }
    }

    private double getLongditude(){
        if(isGetLocation){
            return gps.getLongitude();
        }else{
            return 0;
        }
    }

    private void showSettingAlert(){
        gps.showSettingsAlert();
    }

    public void stopGps(){
        gps.stopUsingGPS();
    }

}
