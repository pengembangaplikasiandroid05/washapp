package wash.org.washapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import wash.org.washapp.R;

/**
 * Created by Mira on 4/23/2017.
 */

public class WashViewHolder extends RecyclerView.ViewHolder {

    public View mView;
    FirebaseAuth mAuth;


    public WashViewHolder(View itemView) {
        super(itemView);
        mView = itemView;
        mAuth=FirebaseAuth.getInstance();
    }

    public void setTitle(String title)
    {
        TextView post_title=(TextView) mView.findViewById(R.id.post_title);
        post_title.setText(title);
    }

    public void setAlamat(String alamat)
    {
        TextView post_alamat=(TextView) mView.findViewById(R.id.post_alamat);
        post_alamat.setText(alamat);
    }

    public void setHarga(String harga)
    {
        TextView post_harga=(TextView) mView.findViewById(R.id.post_harga);
        post_harga.setText(harga);
    }

    public void setImage(final Context c, final String Image)
    {
        final ImageView post_image=(ImageView) mView.findViewById(R.id.post_image);
        Picasso.with(c).load(Image)
                .resize(425,250)
                .centerCrop()
                .networkPolicy(NetworkPolicy.OFFLINE)
                .into(post_image, new Callback() {
                    @Override
                    public void onSuccess() {
                        Picasso.with(c).load(Image).fit().into(post_image);

                    }

                    @Override
                    public void onError() {

                        Picasso.with(c).load(Image).resize(425, 250).into(post_image);

                    }
                });
    }
}