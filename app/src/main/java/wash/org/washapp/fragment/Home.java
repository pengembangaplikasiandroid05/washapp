package wash.org.washapp.fragment;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import wash.org.washapp.R;
import wash.org.washapp.Single;
import wash.org.washapp.Utility;
import wash.org.washapp.Wash;
import wash.org.washapp.adapter.WashViewHolder;
import wash.org.washapp.auth.Login;


/**
 * A simple {@link Fragment} subclass.
 */

public class Home extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    RecyclerView recyclerView;
    private DatabaseReference mData,mDatauser;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAutlistener;
    private Utility mUtility;
    private SwipeRefreshLayout swipeRefreshLayout;

    public Home() {
        // Required empty public constructor
    }

    //
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mUtility=new Utility();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_home, container, false);
        mAuth=FirebaseAuth.getInstance();
        mAutlistener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                if (firebaseAuth.getCurrentUser() == null){
                    Intent loginIntent = new Intent(getActivity().getApplicationContext(), Login.class);
                    loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(loginIntent);
                }
            }
        };

        // Untuk membuat tanda saat di reload //
        swipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipe);
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        swipeRefreshLayout.setRefreshing(true);
                                        onStart();
                                    }
                                }
        );
        mDatauser  = FirebaseDatabase.getInstance().getReference().child("Users");
        mDatauser.keepSynced(true);
        mData = FirebaseDatabase.getInstance().getReference().child("Blog");
        mData.keepSynced(true);

        recyclerView = (RecyclerView)  v.findViewById(R.id.rview);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity().getApplicationContext()));

        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        swipeRefreshLayout.setRefreshing(false);


        final FirebaseRecyclerAdapter<Wash,WashViewHolder> firebaseRecyclerAdapter=new FirebaseRecyclerAdapter<Wash, WashViewHolder>(
                Wash.class,
                R.layout.washlist,
                WashViewHolder.class,
                mData
        ) {

            // Untuk menampilkan holder yang telah di buat sebelumnya //
            @Override
            protected void populateViewHolder(WashViewHolder viewHolder, Wash model, int position) {
                final String post_key=getRef(position).getKey();
                mUtility.idKey.add(new LatLng(Double.valueOf(model.getLat()),Double.valueOf(model.getLng())));
                mUtility.idTitle.add(new String(model.getTitle()));

                viewHolder.setTitle(model.getTitle());
                viewHolder.setAlamat(model.getAlamat());
                viewHolder.setHarga(model.getHarga());
                viewHolder.setImage(getContext(),model.getImage());

                viewHolder.mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent i = new Intent(getActivity().getApplicationContext(), Single.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        i.putExtra("blog_id", post_key);
                        startActivity(i);

                    }
                });
                swipeRefreshLayout.setRefreshing(false);
            }
        } ;
        recyclerView.setAdapter(firebaseRecyclerAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onRefresh() {

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }


    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
