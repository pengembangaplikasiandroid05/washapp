package wash.org.washapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;

import wash.org.washapp.auth.Login;

/**
 * Created by Mira on 4/23/2017.
 */

public class SplashScreenActivity extends AppCompatActivity {
    private FirebaseAuth mAuth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAuth = FirebaseAuth.getInstance();
        if(mAuth.getCurrentUser()!=null)
        {
            startActivity(new Intent(SplashScreenActivity.this,MainActivity.class));
            finish();
        }
        else {
            startActivity(new Intent(SplashScreenActivity.this, Login.class));
        }
        finish();


    }

}
