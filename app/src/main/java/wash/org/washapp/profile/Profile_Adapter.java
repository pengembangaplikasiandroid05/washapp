package wash.org.washapp.profile;

/**
 * Created by Mira on 4/29/2017.
 */

public class Profile_Adapter  {
        private String name;
        private String Image;

        public Profile_Adapter ()
        {

        }


        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getImage() {
            return Image;
        }

        public void setImage(String image) {
            Image = image;
        }
}
