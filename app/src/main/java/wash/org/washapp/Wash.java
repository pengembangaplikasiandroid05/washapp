package wash.org.washapp;

/**
 * Created by Mira on 4/23/2017.
 */
//semua data yang akan ditambahkan maupun yang akan di panggil
public class Wash {
    private String title;
    private String alamat;
    private String Harga;
    private String image;
    private String lat;
    private String lng;
    private String desc;

public Wash()
{

}
    public String getTitle() {
        return title;
    }

    public void setNamatempat(String namatempat) {
        this.title = title;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getHarga() {
        return Harga;
    }

    public void setHarga(String harga) {
        Harga = harga;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
