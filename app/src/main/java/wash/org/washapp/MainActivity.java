package wash.org.washapp;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;

import wash.org.washapp.adapter.TabFragmentPagerAdapter;
import wash.org.washapp.auth.Login;
import wash.org.washapp.fragment.Home;
import wash.org.washapp.fragment.Tempat;
import wash.org.washapp.profile.Profile;

public class MainActivity extends AppCompatActivity {

    //deklarasi semua komponen View yang akan digunakan
    private Toolbar toolbar;
    private ViewPager pager;
    private TabLayout tabs;


    //Deklarasi Firebase
    private FirebaseAuth mAuth;
    private FirebaseDatabase mData;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //title
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //inisialisasi tab dan pager
        pager = (ViewPager) findViewById(R.id.pager);
        tabs = (TabLayout) findViewById(R.id.tabs);
        if(tabs!=null)
            tabs.setupWithViewPager(pager);
        setupViewPager(pager);
        setIcon();

        //inisialisasi Firebase auth untuk menentukan bahwa user masih aktif
        mAuth=FirebaseAuth.getInstance();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    //untuk memberikan nama pada pager atau tabnya
    private void setupViewPager(ViewPager viewPager) {
        TabFragmentPagerAdapter adapter = new TabFragmentPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new Home(), getString(R.string.home));
        adapter.addFragment(new Tempat(), getString(R.string.lokasi));
        viewPager.setAdapter(adapter);

    }

    //untuk menambahkan icon pada pager
    public void setIcon() {
        if (tabs != null) {
            tabs.getTabAt(0).setIcon(getDrawableIcon(0));
            tabs.getTabAt(1).setIcon(getDrawableIcon(1));

        }
    }

    //untuk memanggil gambar yang ada directory drawable
    public int getDrawableIcon(int index) {
        int img[] = {R.drawable.ic_home_black_24dp, R.drawable.ic_directions_car_black_24dp};
        return img[index];
    }

    //untuk memanggil menu yang ada pada pojok kanan atas, yang ada icon seperti titik dua
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu,menu);
        return true;
    }

    //memberikan perintah yang ada pada menu
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.logout) {
            mAuth.signOut();
            Intent i = new Intent(MainActivity.this, Login.class);
            startActivity(i);
            finish();
            return true;
        } else if (id == R.id.Tambahtempat) {
            Intent i = new Intent(this, TambahTempat.class);
            startActivity(i);
            finish();
        }else if(id==R.id.profile)
        {
            Intent i = new Intent(this, Profile.class);
            startActivity(i);
            finish();
        }

        finish();

        return super.onOptionsItemSelected(item);
    }

    // untuk tombol back
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    // untuk aplikasi stop
    @Override
    protected void onStop() {
        super.onStop();

    }
}
