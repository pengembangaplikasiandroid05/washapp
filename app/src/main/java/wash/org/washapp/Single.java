package wash.org.washapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;


// ====================== MENAMPILKAN DETAIL DARI LIST GAMBAR YANG TELAH DIUPLOAD ================ //

public class Single extends AppCompatActivity {
    private String mPost_key = null;

    private DatabaseReference mDatabase;

    private ImageView SingleImage;
    private TextView SingleTitle;
    private TextView SingleDesc;
    private TextView SingleLatlong;
    private Toolbar mToolbar;
    private Button book;

    private FirebaseAuth mAuth;

    // Button untuk menghapus postingan gambar dan detail tapi yang menghapus hanya yang upload saja //
    private Button mSingleRemoveBtn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single);

        //title
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        // Ganti nama tittle //
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        // Untuk Menapilkan image, tittle, description dan latitude //
        SingleImage = (ImageView) findViewById(R.id.singleBlogImage);
        SingleTitle = (TextView) findViewById(R.id.singleBlogTitle);
        SingleDesc = (TextView) findViewById(R.id.singleBlogDesc);
        SingleLatlong = (TextView) findViewById(R.id.singleBlogLatlong);
        book = (Button) findViewById(R.id.booking);
        book.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Single.this, Booking.class));
            }
        });


        mAuth = FirebaseAuth.getInstance();

        mSingleRemoveBtn = (Button) findViewById(R.id.singleRemoveBtn);

        mDatabase = FirebaseDatabase.getInstance().getReference().child("Blog");

        mPost_key = getIntent().getExtras().getString("blog_id");

        // Untuk menampilkan data single //
        mDatabase.child(mPost_key).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String post_title = (String) dataSnapshot.child("title").getValue();
                String post_desc = (String) dataSnapshot.child("desc").getValue();
                String post_latlong = (String) dataSnapshot.child("lat").child("long").getValue();
                String post_image = (String) dataSnapshot.child("image").getValue();
                String post_uid = (String) dataSnapshot.child("uid").getValue();

                SingleTitle.setText(post_title);
                SingleDesc.setText(post_desc);
                SingleLatlong.setText(post_latlong);

                Picasso.with(Single.this).load(post_image).resize(600, 450).into(SingleImage);


                if(mAuth.getCurrentUser().getUid().equals(post_uid)){

                    mSingleRemoveBtn.setVisibility(View.VISIBLE);

                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        mSingleRemoveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mDatabase.child(mPost_key).removeValue();

                Intent mainIntent = new Intent(Single.this, MainActivity.class);
                startActivity(mainIntent);
                finish();

            }
        });

    }

    // Untuk tombol Back //
    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    /** Called when returning to the activity */
    @Override
    public void onResume() {
        super.onResume();

    }


    @Override
    protected void onPause() {
        super.onPause();
    }

    /** Called before the activity is destroyed */
    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
