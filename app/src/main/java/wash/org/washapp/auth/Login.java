package wash.org.washapp.auth;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import wash.org.washapp.MainActivity;
import wash.org.washapp.R;
import wash.org.washapp.Setup;

public class Login extends AppCompatActivity {

    private EditText mLoginEmailField;
    private EditText mLoginPasswordField;
    private Button mLoginBtn;
    private Button mSignupBtn;
    private Button mResetBtn;

    private static final String TAG = Login.class.getSimpleName();

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private DatabaseReference mDatabaseUsers;
    private Signinimpl signinimpl;

    private ProgressBar mProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mAuth = FirebaseAuth.getInstance();

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null){
                    Log.d(TAG, "Sign:in" + user.getUid());
                }else{
                    Log.d(TAG, "Sign:out");
                }
            }
        };

        mProgress = (ProgressBar) findViewById(R.id.progressBarLogin);

        mDatabaseUsers = FirebaseDatabase.getInstance().getReference().child("Users");
        mDatabaseUsers.keepSynced(true);

        mLoginEmailField = (EditText) findViewById(R.id.loginEmailField);
        mLoginPasswordField = (EditText) findViewById(R.id.loginPasswordField);

        mResetBtn = (Button) findViewById(R.id.btn_reset_password);
        mResetBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent resetIntent = new Intent(getApplicationContext(), ResetPasswordActivity.class);
                startActivity(resetIntent);
            }
        });

        mLoginBtn = (Button) findViewById(R.id.loginBtn);

        mLoginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    checkLogin();
                }catch (Exception e){
                    Toast.makeText(Login.this,"Field not complete !",Toast.LENGTH_SHORT).show();
                }

            }
        });

        mSignupBtn = (Button) findViewById(R.id.signupBtn);

        mSignupBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent signUpIntent = new Intent(Login.this, Signup.class);
                startActivity(signUpIntent);
                finish();
            }
        });

    }


    private void checkLogin(){

        final String email = mLoginEmailField.getText().toString().trim();
        final String password = mLoginPasswordField.getText().toString().trim();

        if(TextUtils.isEmpty(email))
        {
            Toast.makeText(getApplicationContext(),"Enter your Email",Toast.LENGTH_LONG).show();
            return;
        }
        else if(TextUtils.isEmpty(password))
        {
            Toast.makeText(getApplicationContext(),"Enter your Password",Toast.LENGTH_LONG).show();
            return;
        }

        else if (password.length() < 6) {
            mLoginPasswordField.setError(getString(R.string.minimum_password));
            return;
        }

        if(!TextUtils.isEmpty(email) && !TextUtils.isEmpty(password)){
            mProgress.setVisibility(View.VISIBLE);
            mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    mProgress.setVisibility(View.GONE);
                    if (task.isSuccessful()){

                        checkUserExist();


                    }else {


                        Toast.makeText(Login.this, getString(R.string.auth_failed), Toast.LENGTH_LONG).show();


                    }

                }
            });

        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void checkUserExist(){
        signinimpl = new Signinimpl(mDatabaseUsers);
        final String user_id = mAuth.getCurrentUser().getUid();

        signinimpl.login(user_id, new ResultLogin() {
            @Override
            public void onSuccess() {
                Intent mainIntent = new Intent(Login.this, MainActivity.class);
                startActivity(mainIntent);
                finish();
            }

            @Override
            public void elseNyaIf() {
                Intent setupIntent = new Intent(Login.this, Setup.class);
                startActivity(setupIntent);
                finish();
            }

            @Override
            public void onCalcel() {
                Log.d(TAG, "onCalcel: "+"");
            }
        });

    }


}


