package wash.org.washapp.auth;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import wash.org.washapp.MainActivity;
import wash.org.washapp.R;
import wash.org.washapp.Setup;

public class Signup extends AppCompatActivity {

    //defining view objects
    private EditText mEmailField;
    private EditText mPasswordField;
    private EditText mReenter;
    private Toolbar mToolbar;

    private Button mRegisterBtn;

    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;

    private ProgressDialog mProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Users");


        //initializing views

        mEmailField = (EditText) findViewById(R.id.emailField);
        mPasswordField = (EditText) findViewById(R.id.passwordField);
        mReenter = (EditText) findViewById(R.id.reEnter);

        mRegisterBtn = (Button) findViewById(R.id.registerBtn);


        mProgress = new ProgressDialog(this);

        //attaching listener to button
        mRegisterBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    startRegister();
                }catch (Exception e){
                    Toast.makeText(Signup.this,"Field not complete !",Toast.LENGTH_SHORT).show();
                }

            }
        });

    }

    public void startRegister(){

        final String email = mEmailField.getText().toString().trim();
        String password = mPasswordField.getText().toString().trim();
        String reEnter = mReenter.getText().toString().trim();
        if(TextUtils.isEmpty(email))
        {
            Toast.makeText(getApplicationContext(),"Enter your Email",Toast.LENGTH_LONG).show();
            return;
        }
        else if(TextUtils.isEmpty(password))
        {
            Toast.makeText(getApplicationContext(),"Enter your Password",Toast.LENGTH_LONG).show();
            return;
        }
        else if(TextUtils.isEmpty(reEnter))
        {
            Toast.makeText(getApplicationContext(),"Please Re-Enter your password",Toast.LENGTH_LONG).show();
        }

        else if (!mPasswordField.getText().toString().equals(mReenter.getText().toString()))
        {
            Toast.makeText(this, "your password not match", Toast.LENGTH_LONG).show();
        }
        else if (password.length() < 6) {
            Toast.makeText(getApplicationContext(), "Password too short, enter minimum 6 characters!", Toast.LENGTH_SHORT).show();
            return;
        }

        else if(!TextUtils.isEmpty(email) && !TextUtils.isEmpty(password)){

            mProgress.setMessage("Signing Up ...");
            mProgress.show();

            mAuth.createUserWithEmailAndPassword(email,password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if(task.isSuccessful()){

                        String user_id = mAuth.getCurrentUser().getUid();
                        DatabaseReference current_user_db = mDatabase.child(user_id);

                        mProgress.dismiss();

                        Intent setupIntent = new Intent(Signup.this, Setup.class);
                        setupIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(setupIntent);
                        finish();

                    }
                }
            });

        }

    }
}
